#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "good_live"
#define PLUGIN_VERSION "0.00"

#include <sourcemod>
#include <sdktools>

public Plugin myinfo = 
{
	name = "Print Clients",
	author = PLUGIN_AUTHOR,
	description = "",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_cprint", Command_CPrint);
}

public Action Command_CPrint(int p_iClient, int p_iArgs)
{
	for (int i = 1; i <= MaxClients + 1; i++)
	{
		if(IsClientValid(i))
		{
			char p_sName[64];
			GetClientName(i, p_sName, sizeof(p_sName));
			PrintToChatAll("Client Index %i Player Name %s", i, p_sName);
		}	
	}	
}

public bool IsClientValid(int p_iClient)
{
	if(p_iClient > 0 && p_iClient <= MaxClients && IsClientInGame(p_iClient))
	{
		return true;
	}
	return false;
}